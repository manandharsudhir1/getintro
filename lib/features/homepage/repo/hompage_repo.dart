import 'package:getintro/features/homepage/data/models/response/user_model.dart';

abstract class HomepageRepo {
  Future<UserModel> getProduct();
}
