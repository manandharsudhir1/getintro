import 'package:get/get.dart';
import 'package:getintro/features/homepage/data/repo/hompage_repo_imp.dart';
import 'package:getintro/features/homepage/presentation/controller/homepage_controller.dart';
import 'package:getintro/features/homepage/repo/hompage_repo.dart';

class HomepageBinding extends Bindings {
  @override
  void dependencies() {
    Get
      ..put<HomepageRepo>(HomepageRepoImp())
      ..put<HomepageController>(HomepageController());
  }
}
