// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    required this.id,
    required this.customerId,
    required this.testId,
    required this.testName,
    required this.isMockTest,
    required this.answers,
    required this.solvedQuestions,
    required this.correctAnswers,
    required this.incorrectAnswers,
    required this.score,
    required this.completedAt,
    required this.completedTime,
    required this.createdAt,
  });

  int id;
  int customerId;
  int testId;
  String testName;
  int isMockTest;
  List<dynamic> answers;
  int solvedQuestions;
  int correctAnswers;
  int incorrectAnswers;
  String score;
  DateTime completedAt;
  String completedTime;
  DateTime createdAt;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        customerId: json["customer_id"],
        testId: json["test_id"],
        testName: json["test_name"],
        isMockTest: json["is_mock_test"],
        answers: List<dynamic>.from(json["answers"].map((x) => x)),
        solvedQuestions: json["solved_questions"],
        correctAnswers: json["correct_answers"],
        incorrectAnswers: json["incorrect_answers"],
        score: json["score"],
        completedAt: DateTime.parse(json["completed_at"]),
        completedTime: json["completed_time"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "customer_id": customerId,
        "test_id": testId,
        "test_name": testName,
        "is_mock_test": isMockTest,
        "answers": List<dynamic>.from(answers.map((x) => x)),
        "solved_questions": solvedQuestions,
        "correct_answers": correctAnswers,
        "incorrect_answers": incorrectAnswers,
        "score": score,
        "completed_at": completedAt.toIso8601String(),
        "completed_time": completedTime,
        "created_at": createdAt.toIso8601String(),
      };
}
