import 'dart:convert';

import 'package:getintro/features/homepage/data/models/response/user_model.dart';
import 'package:getintro/features/homepage/repo/hompage_repo.dart';
import 'package:http/http.dart' as http;

class HomepageRepoImp extends HomepageRepo {
  @override
  Future<UserModel> getProduct() async {
    final response = await http
        .get(Uri.parse("https://topikeps.com/api/v1/front/home"), headers: {
      "Authorization": "Bearer 3210|hwlZbKsz3hi60yJLdCKnPXXhnxBnflMARbjcrACB"
    });
    final decodedData = jsonDecode(response.body);
    final parsedData = UserModel.fromJson(decodedData["user_answer"][0]);
    return parsedData;
  }
}
