import 'package:get/get.dart';
import 'package:getintro/features/homepage/data/models/response/user_model.dart';
import 'package:getintro/features/homepage/repo/hompage_repo.dart';

class HomepageController extends GetxController {
  int count = 0;
  late HomepageRepo homepageRepo;

  UserModel? userModel;
  @override
  void onInit() {
    super.onInit();
    homepageRepo = Get.find<HomepageRepo>();
  }

  Future increase() async {
    userModel = await homepageRepo.getProduct();
    update();
  }
}
