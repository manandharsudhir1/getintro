import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getintro/features/homepage/presentation/controller/homepage_controller.dart';

class Homepage extends StatelessWidget {
  const Homepage({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<HomepageController>();
    return Scaffold(
      body: Center(
        child: GetBuilder<HomepageController>(builder: (_) {
          return Text(
            controller.userModel?.id.toString() ?? "",
            style: Theme.of(context).textTheme.headline1?.copyWith(
                  color: Colors.black,
                ),
          );
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.increase();
        },
        child: Text("+"),
      ),
    );
  }
}
