import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:getintro/features/homepage/di/hompage_binding.dart';
import 'package:getintro/features/homepage/presentation/screens/hompage_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: "/homepage",
      getPages: [
        GetPage(
          name: "/homepage",
          page: () => Homepage(),
          bindings: [
            HomepageBinding(),
          ],
        ),
      ],
    );
  }
}
